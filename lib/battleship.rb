class BattleshipGame
  # attr_reader :player
  attr_accessor :grid, :pos, :mark, :board, :player, :input

  def initialize(player, board)
    @board = board #Board.new(board)
    @player = player
    @grid = grid
  end

  def attack(pos)
    #@board = board
    # x = pos[0]
    # y = pos[-1]
    board[pos] = :x

  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    input = @player.get_play
    input.map!(&:to_i)
    attack(input)
  end

end
