class Board #< Array
  attr_accessor :grid, :pos, :mark

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = self.class.default_grid)
    #default_grid = Array.new(10)
    @grid = grid #|| Board.default_grid
  end

  def display #: prints the board, with marks on any spaces that have been fired upon.
    @grid
  end

  def count #: returns the number of valid targets (ships) remaining
    @grid.flatten.count(:s)
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, val)
    x, y = pos
    grid[x][y] = val
  end

  def populate_grid # to randomly distribute ships across the board
    size = @grid.count
    random_posx = rand(size+1)
    random_posy = rand(size+1)
    size.times { @grid[random_posx, random_posy] = :s }
  end

  def in_range?(pos)
  end

  def full? #when the board is full returns true
    return true if count == @grid.count**2 && count != 0
    false
  end

  def empty?(pos = nil) # when passed a position returns true for an empty position
  # when not passed a position with no ships on the board returns true
    if pos
      x = pos[0]
      y = pos[-1]
      if @grid[x][y]
        return false
      else
        return true
      end
    else
      return false if count > 0
    end
    true
  end

  def place_random_ship #when the board is empty places a ship in a random position
  #   #when the board is empty places ships until the board is full
    if full?
      raise "Board is full"
    elsif empty?
      populate_grid until self.full?
    end
    @grid
  end

  def won? #when no ships remain returns true
    return false if count != 0
    true
  end

end
